# WUST Satellite

Satellite project realized at the Wrocław University of Science and
Technology. The first edition took place in the summer semester 2023/24 as a
part of team project - the original [README](2024-team-project/README.md)
has been moved to the [2024-team-project](2024-team-project) directory.
